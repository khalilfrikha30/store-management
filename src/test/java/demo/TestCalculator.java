package demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.iit.demo.util.CalculatorUtil;

class TestCalculator {

	@Test
	void test() {
		assertThat(CalculatorUtil.sum(1,3)).isEqualTo(4);
	}

}
